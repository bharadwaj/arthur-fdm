import numpy as np # linear algebra
import pandas as pd 
import matplotlib as mpl
import matplotlib.pyplot as plt
    #%matplotlib inline
from subprocess import check_output
#from wordcloud import WordCloud, STOPWORDS
from collections import Counter
import re
#import plotly.plotly as py
import re, string, unicodedata
import nltk
import contractions
from bs4 import BeautifulSoup
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem import LancasterStemmer, WordNetLemmatizer
import gensim #word2vec

dataset = pd.read_csv('/home/mario/Downloads/Datasets/FDM_Unlabeled.csv', error_bad_lines=False)
dataset=dataset.dropna(how='any')   ##to drop if any value in the row has a nan
dataset=dataset.dropna(axis=1, how='all')

words = dataset.stream
words

############ DROPING ROWS #####################                                                                              
dataset.drop(dataset.index[3577], inplace=True)

words = dataset.stream
words.drop(words.index[3577], inplace=True)

###########################Normalization#######################################
from nltk import word_tokenize,sent_tokenize
from nltk.corpus import stopwords
import inflect


def remove_non_ascii(words):
    """Remove non-ASCII characters from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
        new_words.append(new_word)
    return new_words

def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = word.lower()
        new_words.append(new_word)
    return new_words

def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)
    return new_words

def replace_numbers(words):
    """Replace all interger occurrences in list of tokenized words with textual representation"""
    p = inflect.engine()
    new_words = []
    for word in words:
        if word.isdigit():
            new_word = p.number_to_words(word)
            new_words.append(new_word)
        else:
            new_words.append(word)
    return new_words

def remove_stopwords(words):
    """Remove stop words from list of tokenized words"""
    new_words = []
    for word in words:
        if word not in stopwords.words('english'):
            new_words.append(word)
    return new_words

def stem_words(words):
    """Stem words in list of tokenized words"""
    stemmer = LancasterStemmer()
    stems = []
    for word in words:
        stem = stemmer.stem(word)
        stems.append(stem)
    return stems

def lemmatize_verbs(words):
    """Lemmatize verbs in list of tokenized words"""
    lemmatizer = WordNetLemmatizer()
    lemmas = []
    for word in words:
        lemma = lemmatizer.lemmatize(word, pos='v')
        lemmas.append(lemma)
    return lemmas

def normalize(words):
    words = remove_non_ascii(words)
    words = to_lowercase(words)
    words = remove_punctuation(words)
    words = replace_numbers(words)
    words = remove_stopwords(words)
    return words

words = normalize(words)
print(words)

##################vectorization################################################
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score

vectorizer = TfidfVectorizer(stop_words='english')
words = vectorizer.fit_transform(words)

###################Clustering##################################################
#Using the elbow method to find the optimal number of cluster
#############################

from sklearn.cluster import KMeans
wcss = []
for i in range (1,11):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', max_iter = 300, n_init = 5, random_state = 0)
    kmeans.fit(words)
    wcss.append(kmeans.inertia_)
plt.plot(range(1,11),wcss)
plt.title('the Elbow method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()

########################### applying kmeans####################################
kmeans = KMeans(n_clusters = 7,init = 'k-means++', max_iter = 300, n_init = 10, random_state = 0 )
y_kmeans = kmeans.fit_predict(words)

########################converting (numpy.ndarray) to list ####################
kmeans = list(y_kmeans)
###################### converting list to dataframe ###########################
df = pd.DataFrame(kmeans)
###################### concat two dataframe ###################################
dataset1 = pd.concat([dataset,df], axis =1)
dataset.to_csv('dataset_1.csv')
################conversion to csv##############################################
df.to_csv('example.csv')
###################droping the last row in a dataframe ########################
df2.drop(df2.index[len(df2)-1])

##################                                 ############################
words = dataset[(dataset.Cluster == 0)]
clinical = dataset[(dataset.Cluster == 1)]
##########              ######
Nurse = dataset[(dataset.Cluster == 2)]
Medical = dataset[(dataset.Cluster == 3)]
Pharmacist = dataset[(dataset.Cluster == 4)]
President = dataset[(dataset.Cluster == 5)]
Director = dataset[(dataset.Cluster == 6)]

#### concatination of two dataframe ################################
frames = [words,clinical ]
result = pd.concat(frames)

####################################################
words = result

words = df.to_csv('President.csv')
df.to_csv('words.csv', sep='\t', encoding='utf-8')

CD = list(words)


Nurse1 = Nurse.Cluster
Med1 = Medical.Cluster
Pharmacist1 = Pharmacist.Cluster
President1 = President.Cluster
Director1 = Director.Cluster

legend = ['Nurse', 'Medical','Pharmacist','President','Director']
plt.hist([Nurse1, Med1,Pharmacist1,President1,Director1], color=['orange', 'green','red','blue','grey'])
plt.xlabel("Clusters")
plt.ylabel("Frequency")
plt.legend(legend)
plt.title('Cluster distribution')
plt.show()

dataset.columns = ["Bull",'Company Name', 'Location',"Designation","University","Key_contact", "Designation Cluster","Gender"]# renameing column header names

#dataset.columns.replace({1:"Clinical",2:"Nurse",3:"Medical",4:"Pharmacist",5:"President",6:"Director",0:"Other Clinical"},inplace=True)


dataset1 = pd.concat([dataset,df], axis =1)

words = dataset.to_csv('gender_cluster.csv')

dataset=dataset.dropna(how='any')   ##to drop if any value in the row has a nan

dataset = pd.read_csv('/home/lightining/Documents/For_gernder.csv', error_bad_lines=False)

dataset.drop(['Bull'], axis=1)



