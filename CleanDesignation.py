import pandas as pd


class Designation:

    def run_my_class(self):
        db = "./train-data/newDesig.csv"
        data = pd.read_csv(db)
        desig = data['head_line']       # Array holding designations
        desig_dupe = desig      # Secondary designation array to apply in for loop

        # Clean Desig
        # Remove ',' & following strings within design strings
        newdesig = []                    # Hold desig titles without ','
        for r in desig:                    # Loop to remove ',' and following string from desig strings
            str = r
            pos = str.find(',')
            if pos == -1:
                newdesig.append(str)               # If no ',' move onto next row
                continue
            lstr = list(str)
            del lstr[:pos+1]
            str = "".join(lstr)
            newdesig.append(str)
        newdesig = pd.Series(newdesig)   # Convert List into Series

        # Remove 'at' and following strings
        titles = []
        for r in newdesig:
            str = r
            # print(r)
            pos = str.find(' at')
            if pos == -1:
                titles.append(str)
                continue
            lstr = list(str)
            del lstr[pos:]
            str = "".join(lstr)
            titles.append(str)

        titles = pd.Series(titles)  # convert list to series
        print(titles)


if __name__ == "__main__":
    designation = Designation()
    designation.run_my_class()
