#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 13:36:01 2018

@author: mario
"""

import pandas as pd
#from sklearn.cross_validation import train_test_split
#For multinomial 
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
import random

#loading dataset
data = pd.read_csv("/home/mario/Downloads/Datasets/Labeled_data_FDM_P&T.csv")
data_unlabeled= pd.read_csv("/home/mario/Downloads/Datasets/FDM_Unlabeled.csv")

#dealing with nan values

new_data = data.fillna({'skills': 'No skill','degree' : 'No degree', 'stream':'No stream','designation':'No designation','company':'No company' })
new_data_nan_unlabeled = data_unlabeled.fillna({'skills': 'No skill','degree' : 'No degree', 'stream':'No stream','designation':'No designation','company':'No company' })

#Assigning values
y_train = new_data[['Target']].values
X_train = (new_data["designation"].map(str) + " "+ new_data["skills"] +" "+ new_data["degree"]+" "+ new_data['stream']).values.tolist()
X_test = (new_data_nan_unlabeled["designation"].map(str) + " "+ new_data_nan_unlabeled["skills"] +" "+ new_data_nan_unlabeled["degree"]+" "+ new_data_nan_unlabeled['stream']).values.tolist()

#vectorizing and navie bayes
model = make_pipeline(TfidfVectorizer(), MultinomialNB())
model.fit(X_train, y_train)
predicted_y = model.predict(X_test)

#concatenation and coverting to dataframe
predicted_y = list(predicted_y)
predicted_y = pd.DataFrame(predicted_y)
#AB = X_test +  predicted_y  
predicted_y.columns = ['Target']
X_test = pd.DataFrame(X_test)
X_test.columns = ['Description']
X_test["Target"] = predicted_y["Target"]

