#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 11:20:31 2018

@author: mario
"""

import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

phd = pd.read_csv('/home/mario/Downloads/Edu_phd.csv', sep="\n", error_bad_lines=False, header=None)
phd['target'] = 1

pharmd = pd.read_csv('/home/mario/Downloads/Edu_pharmd.csv', sep="\n",error_bad_lines=False, header=None)
pharmd['target'] = 2

master = pd.read_csv('/home/mario/Downloads/Edu_masters.csv', sep="\n",error_bad_lines=False, header=None)
master['target'] = 3

bachelor = pd.read_csv('/home/mario/Downloads/Edu_bachelors.csv', sep="\n",error_bad_lines=False, header=None)
bachelor['target'] = 4

other = pd.read_csv('/home/mario/Downloads/Edu_others.csv', sep="\n",error_bad_lines=False, header=None)
other['target'] = 5

phd = phd.iloc[1:12]
pharmd = pharmd.iloc[1:12]
master = master.iloc[1:12]
bachelor = bachelor.iloc[1:12]
other = other.iloc[1:12]
# Join all corpus.
#stream_fields = pd.concat([Admin, Health, Med, otherz, phar], axis = 0)
data_fields = pd.concat([phd, pharmd, master, bachelor, other], axis = 0)

# Randomize indexes in the df.
#stream_fields.to_csv("out1.csv")
data_fields = data_fields.sample(frac=1)
# Rename columns.
data_fields.columns = ['Degree', 'target']
# Split to train test datasets. 
cutoff = round(data_fields.shape[0] * 0.75)
fields_train = data_fields.iloc[:cutoff]
fields_test = data_fields.iloc[cutoff:]

# Naive Bayes Classifier.
text_clf = Pipeline([('vect', CountVectorizer()),
                     #('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])

text_clf = text_clf.fit(fields_train.Degree, fields_train.target)

predicted = text_clf.predict(fields_test.Degree)
print('Accuracy of MNB Classifier: ' + str(np.mean(predicted == fields_test.target)))
mnbpredicted = text_clf.predict(["Bachelor"])


# Support Vector Machine
text_clf_svm = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf-svm', SGDClassifier(loss='hinge', penalty='l2',
                                                   alpha=1e-3, n_iter=5, random_state=42)),
                                                   ])
text_clf_svm.fit(fields_train.Degree, fields_train.target)
predicted_svm = text_clf_svm.predict(fields_test.Degree)
print('Accuracy of SVM Classifier: '+ str(np.mean(predicted_svm == fields_test.target)))
svmpredicted = text_clf_svm.predict(["Clinical Mental & Physical Health"])

# Hybrid Grid and SVM

parameters_svm = {'vect__ngram_range': [(1, 1), (1, 2)],
                                        'tfidf__use_idf': (True, False),
                                        'clf-svm__alpha': (1e-2, 1e-3),}
param_grid={
    'count__analyzer': ['word', 'char', 'char_wb'],
    'count__ngram_range': [(1,1), (1,2), (1,3), (1,4), (1,5), (2,3)],
    'onehot__threshold': [0.0, 1.0, 2.0, 3.0],
    'bayes__alpha': [0.0, 1.0],
}
gs_clf_svm = GridSearchCV(text_clf_svm, parameters_svm, n_jobs=-1)
gs_clf_svm = gs_clf_svm.fit(fields_train.Degree, fields_train.target)
print('Accuracy of SVM GridSearch: ' + str(gs_clf_svm.best_score_))
svmgspredicted = text_clf_svm.predict(["Bachelor"])
# gs_clf_svm.best_params_
predicted_y = text_clf_svm.predict(fields_test)
pred_grid_svm = gs_clf_svm.predict(fields_test.Degree)


#pred_grid_svm.to_csv("degree_grid_prediction_SVM.csv")
prediction = pd.DataFrame(pred_grid_svm, columns=['predictions']).to_csv('degree_grid_prediction_SVM.csv')
