import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

Administration = pd.read_csv('/home/mario/Downloads/administration.csv', header=None)
Administration['target'] = 1

pharmacy = pd.read_csv('/home/mario/Downloads/pharmacy.csv', header=None)
pharmacy['target'] = 2

Medicine = pd.read_csv('/home/mario/Downloads/medicine.csv', header=None)
Medicine['target'] = 3

others = pd.read_csv('/home/mario/Downloads/others.csv',  header=None)
others['target'] = 4

pharmacy = pd.read_csv('/home/mario/Downloads//bull.csv',  header=None)
pharmacy['target'] = 2

fields = pd.concat([Administration, pharmacy, Medicine, others, pharmacy], axis = 0)

fields.columns = ['degree', 'target']

fields = fields.sample(frac=1)

fields.to_csv("degree.csv")

cutoff = round(fields.shape[0] * 0.75)
fields_train = fields.iloc[:cutoff]
fields_test = fields.iloc[cutoff:]

# Naive Bayes Classifier.
text_clf = Pipeline([('vect', CountVectorizer()),
                     #('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])

text_clf = text_clf.fit(fields_train.degree, fields_train.target)

predicted = text_clf.predict(fields_test.degree)
print('Accuracy of MNB Classifier: ' + str(np.mean(predicted == fields_test.target)))
mnbpredicted = text_clf.predict(["Human Services Administration"])


# Support Vector Machine
text_clf_svm = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf-svm', SGDClassifier(loss='hinge', penalty='l2',
                                                   alpha=1e-3, n_iter=5, random_state=42)),
                                                   ])
text_clf_svm.fit(fields_train.degree, fields_train.target)
predicted_svm = text_clf_svm.predict(fields_test.degree)
print('Accuracy of SVM Classifier: '+ str(np.mean(predicted_svm == fields_test.target)))
svmpredicted = text_clf_svm.predict(["Doctor of Medicine (MD)"])

# Hybrid Grid and SVM

parameters_svm = {'vect__ngram_range': [(1, 1), (1, 2)],
                                        'tfidf__use_idf': (True, False),
                                        'clf-svm__alpha': (1e-2, 1e-3),}
param_grid={
    'count__analyzer': ['word', 'char', 'char_wb'],
    'count__ngram_range': [(1,1), (1,2), (1,3), (1,4), (1,5), (2,3)],
    'onehot__threshold': [0.0, 1.0, 2.0, 3.0],
    'bayes__alpha': [0.0, 1.0],
}
gs_clf_svm = GridSearchCV(text_clf_svm, parameters_svm, n_jobs=-1)
gs_clf_svm = gs_clf_svm.fit(fields_train.degree, fields_train.target)
print('Accuracy of SVM GridSearch: ' + str(gs_clf_svm.best_score_))
svmgspredicted = text_clf_svm.predict(["Pre-Medicine/Pre-Medical Studies and English"])
# gs_clf_svm.best_params_
predicted_y = text_clf_svm.predict(fields_test)
pred_grid_svm = gs_clf_svm.predict(fields_test.degree)


pred_grid_svm.to_csv("degree_pred_grid_svm.csv")
prediction = pd.DataFrame(pred_grid_svm, columns=['predictions']).to_csv('degree_pred_grid_svm.csv')


