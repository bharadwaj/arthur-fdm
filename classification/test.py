#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 16:13:32 2018

@author: mario
"""

import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
#For multinomial 
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

data = pd.read_csv("/home/mario/Downloads/ML/classification/out1.csv")
data.dropna()
data = data.sample(frac=1)
data.columns = ['bull','Stream', 'target']

#Reading X and y variables.

y = data['target'].values
X = data['Stream'].astype(str).values.tolist()


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

model = make_pipeline(TfidfVectorizer(), MultinomialNB())

model.fit(X_train, y_train)
predicted_y = model.predict(X_test)

#Confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test,predicted_y)

print('Accuracy of MNB Classifier: '+ str(np.mean(predicted_y == y_test)))

text_clf_svm = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf-svm', SGDClassifier(loss='hinge', penalty='l2',
                                                   alpha=1e-3, n_iter=5, random_state=42)),
                                                   ])
text_clf_svm.fit(X_train, y_train)
predicted_svm = text_clf_svm.predict(X_test)
cm = confusion_matrix(y_test,predicted_svm)
print('Accuracy of SVM Classifier: '+ str(np.mean(predicted_svm == y_test)))

