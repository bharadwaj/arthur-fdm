import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
#For multinomial 
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

data = pd.read_csv("/home/mario/Downloads/Datasets/Labeled_data_FDM_P&T.csv")
#data_unlabeled= pd.read_csv("/home/mario/Downloads/Datasets/FDM_Unlabeled.csv")

new_data = data.fillna({'skills': 'No skill','degree' : 'No degree', 'stream':'No stream','designation':'No designation','company':'No company' })
#new_data_nan_unlabeled = data_unlabeled.fillna({'skills': 'No skill','degree' : 'No degree', 'stream':'No stream','designation':'No designation','company':'No company' })

#Assigning values
y= new_data[['Target']].values
X= (new_data["designation"].map(str) +" "+ new_data['stream']).values.tolist()
#X_test = (new_data_nan_unlabeled["designation"].map(str) + " "+ new_data_nan_unlabeled["skills"] +" "+ new_data_nan_unlabeled["degree"]+" "+ new_data_nan_unlabeled['stream']).values.tolist()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

model = make_pipeline(TfidfVectorizer(), MultinomialNB())

model.fit(X_train, y_train)
predicted_y = model.predict(X_test)

#Confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test,predicted_y)

print('Accuracy of MNB Classifier: '+ str(np.mean(predicted_y == y_test)))

text_clf_svm = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf-svm', SGDClassifier(loss='hinge', penalty='l2',
                                                   alpha=1e-3, n_iter=5, random_state=42)),
                                                   ])
text_clf_svm.fit(X_train, y_train)
predicted_svm = text_clf_svm.predict(X_test)
cm = confusion_matrix(y_test,predicted_svm)
print('Accuracy of SVM Classifier: '+ str(np.mean(predicted_svm == y_test)))

