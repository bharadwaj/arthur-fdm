import numpy as np
import pandas as pd
from sklearn.svm import SVC


def classify():
    data = pd.read_csv("./corpora/ExampleOutput.csv")
    a = np.array(data)
    y = a[:, 2]
    x = np.column_stack((data.Designation, data.HighestDegree))
    x.shape
    clf = SVC(probability=True, kernel='linear')
    clf.fit(x, y)
    print(clf.score(x, y, sample_weight=None))
    print(clf.predict([[5, 2]]))
    # print(clf.predict_proba([[5, 1]]))


if __name__ == "__main__":
    classify()
