import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
import MySQLdb


def load_designation_fields_corpus():
    clinical = pd.read_csv('./corpora/designation_field/field_clinical.txt', sep="\n", header=None)
    clinical['target'] = 1

    medical = pd.read_csv('./corpora/designation_field/field_medical.txt', sep="\n", header=None)
    medical['target'] = 2

    nurse = pd.read_csv('./corpora/designation_field/field_nurs.txt', sep="\n", header=None)
    nurse['target'] = 3

    others = pd.read_csv('./corpora/designation_field/field_others.txt', sep="\n", header=None)
    others['target'] = 4

    pharma = pd.read_csv('./corpora/designation_field/field_pharm.txt', sep="\n", header=None)
    pharma['target'] = 5

    # Join all corpus.
    designation_fields = pd.concat([clinical, medical, nurse, others, pharma], axis=0)
    # Randomize indexes in the df.
    designation_fields = designation_fields.sample(frac=1)
    # Rename columns.
    designation_fields.columns = ['text', 'target']
    # Split to train test datasets.
    cutoff = round(designation_fields.shape[0] * 0.75)
    fields_train = designation_fields.iloc[:cutoff]
    fields_test = designation_fields.iloc[cutoff:]
    return fields_train, fields_test


def load_education_streams_corpus():
    administration = pd.read_csv('./corpora/education_stream/stream_administration.txt', sep="\n", header=None)
    administration['target'] = 1

    health = pd.read_csv('./corpora/education_stream/stream_health.txt', sep="\n", header=None)
    health['target'] = 2

    medicine = pd.read_csv('./corpora/education_stream/stream_medicine.txt', sep="\n", header=None)
    medicine['target'] = 3

    others = pd.read_csv('./corpora/education_stream/stream_others.txt', sep="\n", header=None)
    others['target'] = 4

    pharma = pd.read_csv('./corpora/education_stream/stream_pharma.txt', sep="\n", header=None)
    pharma['target'] = 5

    # Join all corpus.
    designation_fields = pd.concat([administration, health, medicine, others, pharma], axis=0)
    # Randomize indexes in the df.
    designation_fields = designation_fields.sample(frac=1)
    # Rename columns.
    designation_fields.columns = ['text', 'target']
    # Split to train test datasets.
    cutoff = round(designation_fields.shape[0] * 0.75)
    fields_train = designation_fields.iloc[:cutoff]
    fields_test = designation_fields.iloc[cutoff:]
    return fields_train, fields_test


def init_classifier_multi_naive_bayes(train_data):
    """Not Being Used. Yields 80% Accuracy."""
    text_clf = Pipeline([('vect', CountVectorizer()),
                         # ('tfidf', TfidfTransformer()),
                         ('clf', MultinomialNB()),
                         ])

    return text_clf.fit(train_data.text, train_data.target)


def init_classifier_svm(train_data, test_data):
    text_clf_svm = Pipeline([('vect', CountVectorizer()),
                             # ('tfidf', TfidfTransformer()),
                             ('clf-svm', SGDClassifier(loss='hinge', penalty='l2',
                                                       alpha=1e-3, max_iter=5, tol=None, random_state=42)),
                             ])
    text_clf_svm.fit(train_data.text, train_data.target)
    predicted_svm = text_clf_svm.predict(test_data.text)
    print('SVM Text Classifier Accuracy: ' + str(np.mean(predicted_svm == test_data.target)))
    return text_clf_svm


def init_grid_search_cv_svm(text_clf_svm, train_data):
    """For further optimization of SVM. But yielding no much results."""
    parameters_svm = {'vect__ngram_range': [(1, 1), (1, 2)],
                      'tfidf__use_idf': (True, False),
                      'clf-svm__alpha': (1e-2, 1e-3), }
    gs_clf_svm = GridSearchCV(text_clf_svm, parameters_svm, n_jobs=-1)
    gs_clf_svm = gs_clf_svm.fit(train_data.text, train_data.target)
    print('SVM GridSearch Accuracy: ' + str(gs_clf_svm.best_score_))
    return gs_clf_svm


def init_field_classifier():
    fields_train, fields_test = load_designation_fields_corpus()
    svm_text_classifier = init_classifier_svm(fields_train, fields_test)
    # gs_svm_classifier = init_grid_search_cv_svm(svm_text_classifier, fields_train)
    # print(gs_svm_classifier.predict(["Chief medical Officer"])[0])
    return svm_text_classifier


def init_stream_classifier():
    streams_train, streams_test = load_education_streams_corpus()
    svm_text_classifier = init_classifier_svm(streams_train, streams_test)
    # gs_svm_classifier = init_grid_search_cv_svm(svm_text_classifier, fields_train)
    # print(gs_svm_classifier.predict(["Chief medical Officer"])[0])
    return svm_text_classifier


def all_fdm_ids_of_pt_committee(cursor):
    # execute SQL query using execute() method.
    # cursor.execute("SELECT id, PTCommitteeMember FROM org_keycontacts where PTCommitteeMember = 'Yes'")
    # cursor.execute("SELECT distinct contact_id  FROM `org_experience` WHERE contact_id IN (SELECT id FROM " +
    #               "org_keycontacts WHERE PTCommitteeMember='Yes')")
    cursor.execute("select 	o.KeyContacts, o.id, o.TitleRole,s.degree, s.stream from sunovion20180706.org_keycontacts o "
                   + " inner join sunovion20180706.org_school_attended s "
                   + " on o.id = s.contact_id "
                   + " where PTCommitteeMember ='Yes'")
    return cursor.fetchall()


def find_fdm_designation_by_id(cursor, fdm):
    cursor.execute("SELECT id, designation FROM org_experience where id = " + str(fdm[0]))
    return cursor.fetchall()


def update_fdm_field(cursor, field_label, fdm_id):
    update_sql = "UPDATE org_keycontacts SET field = %d WHERE id = %d", (field_label, fdm_id[0])
    cursor.execute(update_sql)


if __name__ == "__main__":
    # Initialize Database Connection.
    db = MySQLdb.connect(host="183.82.108.170", port=7076, user="root", passwd="root", db="sunovion20180706")
    cursor = db.cursor()
    # Initialize our Text Classifier. Fetches 94% accuracy.
    field_clf = init_field_classifier()
    stream_clf = init_stream_classifier()
    # Uncomment and play with this :)
    # label = field_clf.predict(["Senior Director, Hospital and Risk Based Contracting"])[0]
    all_fdms_with_pt = all_fdm_ids_of_pt_committee(cursor)
    # Classify each fdm individually and update in database.
    for fdm in all_fdms_with_pt:
        # fdm_designation = find_fdm_designation_by_id(cursor, fdm)
        predicted_field = ''
        predicted_stream = ''
        fdm_designation = fdm
        if fdm_designation[2]:
            predicted_field = field_clf.predict([fdm_designation[2]])[0]
        if fdm_designation[4]:
            predicted_stream = stream_clf.predict([fdm_designation[4]])[0]

        print(str(fdm[0]) + ", id: " + str(fdm_designation[1]) + ", field: " + 
              str(predicted_field)+ ", stream: " + str(predicted_stream))
        
        # Uncomment and update only when you are confident.
        # update_fdm_field(cursor, predicted_field, fdm)

    db.close()
    
