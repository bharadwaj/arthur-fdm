import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

clinical = pd.read_csv('./corpora/designation_field/field_clinical.txt', sep="\n", header=None)
clinical['target'] = 1

medical = pd.read_csv('./corpora/designation_field/field_medical.txt', sep="\n", header=None)
medical['target'] = 2

nurse = pd.read_csv('./corpora/designation_field/field_nurs.txt', sep="\n", header=None)
nurse['target'] = 3

others = pd.read_csv('./corpora/designation_field/field_others.txt', sep="\n", header=None)
others['target'] = 4

pharma = pd.read_csv('./corpora/designation_field/field_pharm.txt', sep="\n", header=None)
pharma['target'] = 5

# Join all corpus.
designation_fields = pd.concat([clinical, medical, nurse, others, pharma], axis = 0)
# Randomize indexes in the df.
designation_fields = designation_fields.sample(frac=1)
# Rename columns.
designation_fields.columns = ['designation', 'target']
# Split to train test datasets. 
cutoff = round(designation_fields.shape[0] * 0.75)
fields_train = designation_fields.iloc[:cutoff]
fields_test = designation_fields.iloc[cutoff:]

# Naive Bayes Classifier.
text_clf = Pipeline([('vect', CountVectorizer()),
                     #('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])

text_clf = text_clf.fit(fields_train.designation, fields_train.target)

predicted = text_clf.predict(fields_test.designation)
print('Accuracy of MNB Classifier: ' + str(np.mean(predicted == fields_test.target)))
mnbpredicted = text_clf.predict(["Director Of Clinical"])


# Support Vector Machine
text_clf_svm = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf-svm', SGDClassifier(loss='hinge', penalty='l2',
                                                   alpha=1e-3, n_iter=5, random_state=42)),
                                                   ])
text_clf_svm.fit(fields_train.designation, fields_train.target)
predicted_svm = text_clf_svm.predict(fields_test.designation)
print('Accuracy of SVM Classifier: '+ str(np.mean(predicted_svm == fields_test.target)))
svmpredicted = text_clf_svm.predict(["Chief Medical Officer"])

# Hybrid Grid and SVM

parameters_svm = {'vect__ngram_range': [(1, 1), (1, 2)],
                                        'tfidf__use_idf': (True, False),
                                        'clf-svm__alpha': (1e-2, 1e-3),}
param_grid={
    'count__analyzer': ['word', 'char', 'char_wb'],
    'count__ngram_range': [(1,1), (1,2), (1,3), (1,4), (1,5), (2,3)],
    'onehot__threshold': [0.0, 1.0, 2.0, 3.0],
    'bayes__alpha': [0.0, 1.0],
}
gs_clf_svm = GridSearchCV(text_clf_svm, parameters_svm, n_jobs=-1)
gs_clf_svm = gs_clf_svm.fit(fields_train.designation, fields_train.target)
print('Accuracy of SVM GridSearch: ' + str(gs_clf_svm.best_score_))
svmgspredicted = text_clf_svm.predict(["Chief of Pharmacy Officer"])
# gs_clf_svm.best_params_